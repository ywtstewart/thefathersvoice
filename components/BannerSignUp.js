const Banner = (props) => (
    <section id="banner" className="major sign-up">
        <div className="inner">
            <header className="major">
                <h1>Great! See you there!</h1>
            </header>
            <div className="content">
            </div>
        </div>
    </section>
)

export default Banner
