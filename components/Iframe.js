import React, { Component } from 'react'

export default class Iframe extends Component {
    constructor(props) {
        super(props);
    }

    iframe = () => {
        return {
            __html: this.props.iframe
        }
    }
    render() {
        return (
            <div>
                <div dangerouslySetInnerHTML={this.iframe()} />
            </div>
        )
    }
}
