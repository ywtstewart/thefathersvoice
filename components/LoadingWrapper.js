import React, { Component } from 'react'
import styled, { keyframes } from "styled-components";
import Loading from "./Loading";

const SectionContainer = styled.div`
    background-color: #fff;
    display: flex;
    justify-content: center;
    padding: 40px 0;
`

export default class LoadingWrapper extends Component {
    constructor(props) {
        super();

        this.state = {
            hidden: true
        }
    }


    componentWillMount = () => {
        var that = this;
        setTimeout(function () {
            that.show();
        }, this.props.delay ? this.props.delay : 5000);
    }

    show = () => {
        this.setState({ hidden: false });
    }
    render() {
        return (
            <SectionContainer>
                {this.state.hidden &&
                    <Loading />
                }

                {!this.state.hidden &&
                    <React.Fragment>
                        {this.props.children}
                    </React.Fragment>
                }
            </SectionContainer>
        )
    }
}


