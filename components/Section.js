import React from "react";
import styled, { css } from "styled-components";

const Section = styled.div`
    align-items: center;
    display: flex;
    justify-content: center;
    flex-wrap: nowrap;
    padding: 40px 20px;
    background: ${props => props.alternate == true ? "#306AFC" : "#fff"};
    color: ${props => props.alternate ? "#fff" : "#333"}

    ${(props) => props.alternate && `
        background-image: url("../static/images/clouds-bg.png");
        background-attachment: fixed;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        border-bottom: 0 !important;
        cursor: default;
        position: relative;
        z-index:0;
        &:after {
            background-color: #306AFC;
            content: '';
            display: block;
            height: 100%;
            left: 0;
            opacity: 0.55;
            position: absolute;
            top: 0;
            width: 100%;
            z-index: -1;
        }
    `}

`

const SectionInner = styled.div`
    max-width: 1440px;
    width: 100%;
`


export default (props) => {
    return (
        <Section {...props}>
            <SectionInner>
                {props.children}
            </SectionInner>
        </Section>
    )
}
