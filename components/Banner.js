import Link from "next/link";

const Banner = (props) => (
    <section id="banner" className="major">
        <div className="inner">
            <header className="major">
                <h1>The Fathers Voice Conference</h1>
            </header>
            <div className="content">

                <ul className="actions">

                    <li><a href="https://www.ticketkantoor.nl/shop/Conference" className="button">Sign up!</a></li>
                </ul>
            </div>
        </div>
    </section>
)

export default Banner
