import React from "react";
import styled from "styled-components";

const Icons = styled.ul`
    margin: 0;
    padding: 20px 0;
    text-align: center;
    list-style: none;
    display: flex;
    justify-content: center;

`


const Footer = (props) => (
    <footer id="footer">
        <Icons>
            <li><a href="https://twitter.com/hdlife010" className="icon alt fa-twitter"><span className="label">Twitter</span></a></li>
            <li><a href="https://www.facebook.com/HeavenDesignedLife/" className="icon alt fa-facebook"><span className="label">Facebook</span></a></li>
            <li><a href="https://www.instagram.com/hdlife010/" className="icon alt fa-instagram"><span className="label">Instagram</span></a></li>
        </Icons>
    </footer>
)

export default Footer
