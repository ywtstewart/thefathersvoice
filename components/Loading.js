import React from 'react'
import styled, { keyframes } from "styled-components";

const anim = keyframes`
    0% { 
        transform: scale(0);
    }
    40% { 
        transform: scale(1.0);
    }
    80%  { 
        transform: scale(0);
    }
    100%  { 
        transform: scale(0);
    }
}`

const Spinner = styled.div`
    text-align: left;   



`

const Bounce = styled.div`
    width: 18px;
    height: 18px;
    background-color: #306AFC;
    display: inline-block;
    border-radius: 100%;
    transition: all ease-in-out 3s;
    animation: ${anim} 1.4s infinite ease-in-out both;  
`

const Bounce1 = styled(Bounce)`
    animation-delay: -0.48s;
`

const Bounce2 = styled(Bounce)`
    animation-delay: -0.32s;
`

const Bounce3 = styled(Bounce)`
animation-delay: -0.16s;
`
export default () => {
    return (
        <Spinner>
            <Bounce1></Bounce1>
            <Bounce2></Bounce2>
            <Bounce3></Bounce3>
        </Spinner>
    )
}
