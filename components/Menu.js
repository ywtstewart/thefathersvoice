import PropTypes from 'prop-types'
import Link from 'next/link'

const Menu = (props) => (
    <nav id="menu">
        <div className="inner">
            <ul className="links">
                <li><Link href="/"><a><span onClick={props.onToggleMenu}>Home</span></a></Link></li>
                <li><a href="https://www.ticketkantoor.nl/shop/Conference"><span onClick={props.onToggleMenu}>Sign up</span></a></li>
            </ul>
            <ul className="actions vertical">
                <li><a href="https://www.hd-life.org" className="button special fit">Visit HD-Life.org</a></li>
            </ul>
        </div>
        <a className="close" onClick={props.onToggleMenu} href="javascript:;">Close</a>
    </nav>
)

Menu.propTypes = {
    onToggleMenu: PropTypes.func
}

export default Menu
