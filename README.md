# The Fathers Voice Conference Site made with nextjs-starter-forty
A Next.js starter based on the Forty site template, designed by HTML5 UP. Check out https://codebushi.com/nextjs-website-starters/ for more Next.js starters and templates.

## Preview


## Getting Started

To get started, simply clone the repository and run `npm install` or `yarn`

```

# Install npm packages
npm install

# Start up the next.js dev server, browse to http://localhost:3000/
npm run dev
```

## Generating the Static Site

```
# Build and export the site, the static files are generated in the out/ folder
npm run export
```