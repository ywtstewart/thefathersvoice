import Layout from '../components/Layout'
import Banner from '../components/BannerSignUp'
import Section from "../components/LoadingWrapper";
import Iframe from "../components/Iframe";
import React from 'react'
import styled from 'styled-components';
const iframe = "<iframe src='https://www.ticketkantoor.nl/shop/Conference' frameborder='0'></iframe>"

const IframeContainer = styled.div`
    display: inline-block
    position: relative;
    overflow: hidden;
    padding-bottom: 1400px;
    width: 100%;
    max-width: 1200px;

    iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        border: 0;
    }
`

const SignUp = () => {

    return (
        <React.Fragment>
            <Layout>
                <Banner />
                <Section delay={1}>
                    <IframeContainer>
                        <Iframe iframe={iframe} />
                    </IframeContainer>
                </Section>
            </Layout>
        </React.Fragment>

    )
}

export default SignUp;