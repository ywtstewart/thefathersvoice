import Link from 'next/link'

import Layout from '../components/Layout'
import Banner from '../components/Banner'
import styled from "styled-components";
import Section from "../components/Section";
import { device } from "../components/responsive"

const Main = styled.div`
    display: block;
    color: #333;
    background-color: #fff;
`
const Title = styled.h2`
    width: 100%;
    color: ${props => props.alternate ? "#fff" : "#306AFC"};
    font-size: 40px;
    text-align: center;
    flex: 1;
    padding: 20px 0;

`;

const Subtitle = styled.div`
font-size: 20px;
`;

const Intro = styled.div`
    display: flex;
    flex-flow: row wrap;

`;

const IntroText = styled.div`
    text-align: center;
    margin: 0 auto;
    max-width: 700px;
    padding: 80px 0;
    font-size: 22px;
    line-height: 24px;
    color: #333;

`

const HostText = styled.div`
text-align: center;
padding: 20px 0;
flex: 1 100%;
font-size: 18px;

@media ${device.tablet} {
    flex-basis: 50%;
    }
    
`;

const HostDescription = styled.div`

`;


const HostName = styled.h3`
    flex: 1;
    color: #306AFC;
`;

const HostImage = styled.div`
    flex: 1 50%;
    text-align: center;

    img {
        max-width: 100%;
        border-radius: 100%;
        overflow: hidden;
        border: 8px solid #306AFC;
    }
`;

const Speakers = styled.div`
display: flex;
flex-flow: row wrap;


`;

const Speaker = styled.div`
    width: 100%;

    @media ${device.tablet} {
        width: ${100 / 4}%;
        flex-basis: ${100 / 4}%;
    }

`;

const SpeakerImage = styled.div`
    flex: 1;
    text-align: center;
    overflow: hidden;

    img {
        border-radius: 50%;
        border: 8px solid #fff;
        width: 200px;
        margin: 0 auto;
    }
`;

const SpeakerName = styled.div`
    text-align: center;
    padding: 20px 0;
`;
const PriceBlock = styled.div`
    border-radius: 15px;
    overflow: hidden;
    display: flex;
    flex-flow: row wrap;
    margin: 0 auto;
    width: 325px;
    max-width: 100%;
    border: 1px solid #306AFC;
    text-align: center;
    background-color: #fff;
    color: #333;

`;

const Price = styled.div`
    padding: 40px 0;
    font-size: 58px;
    width: 100%;
    color: #fff;
    background-color: #306AFC;
    border-bottom: 1px solid #306AFC;
`;

const PriceText = styled.div`
padding: 20px;
text-align: left;

ul {
    li {
        padding: 0;
        margin: 0;
    }
}

`;

const Disclaimer = styled.div`
    font-size: 12px;
    line-height: 14px;
    font-style: italic;
`;
const ButtonWrapper = styled.div`
    text-align: center;
    width: 100%;
    padding: 20px 0;
    border-top: 1px solid #306AFC;
`
export default () => (
    <Layout>
        <div>
            <Banner />
            <Main>
                <Section className="#about">
                    <IntroText>
                        <p>“This conference helps us to hear the voice of our Father <br />
                            to become even more rooted in Him and His Word.”</p>
                    </IntroText>
                </Section>
                <Section alternate>
                    <Title alternate>Speakers</Title>
                    <Speakers>
                        <Speaker>
                            <SpeakerImage>
                                <img src="/static/images/speaker-melvin.jpg" width="50%" alt="Hosts Stefan and" />
                            </SpeakerImage>
                            <SpeakerName>Apostle Melvin Abrahams<br />(South Africa)</SpeakerName>
                        </Speaker>
                        <Speaker>
                            <SpeakerImage>
                                <img src="/static/images/speaker-jehonathan.jpg" width="50%" alt="Hosts Stefan and" />
                            </SpeakerImage>
                            <SpeakerName>Apostle Jehonathan<br />(South Africa)</SpeakerName>
                        </Speaker>
                        <Speaker>
                            <SpeakerImage>
                                <img src="/static/images/speaker-daryl.jpg" width="50%" alt="Hosts Stefan and" />
                            </SpeakerImage>
                            <SpeakerName>Apostle Daryl O’Neil<br />(USA)</SpeakerName>
                        </Speaker>
                        <Speaker>
                            <SpeakerImage>
                                <img src="/static/images/speaker-arleen.jpg" width="50%" alt="Hosts Stefan and" />
                            </SpeakerImage>
                            <SpeakerName>Apostle Arleen Westerhof<br />(Amsterdam)</SpeakerName>
                        </Speaker>
                    </Speakers>
                </Section>

                <Section>
                    <Title>Hosts</Title>
                    <Intro>
                        <HostText>
                            <HostName>Apostle Stefan & Prophetess Melvina Lakhichand</HostName>
                            <HostDescription>
                                This annual Father’s Voice conference is one of the most exciting times we all look forward to. In this age of the apostolic and prophetic ministry the voice of our Heavenly Father and the guidance of the Holy Spirit is of the utmost importance.<br /><br /> As disciples we need the source in our lives, we need to understand what He wants from us in time. Coming to the conference will guarantee you another level of the revelatory Word of God and impartation through the Holy Spirit.
                            </HostDescription>
                        </HostText>
                        <HostImage>
                            <img src="/static/images/hosts.jpg" width="400px" alt="Hosts Stefan and Malvina" />
                        </HostImage>
                    </Intro>
                </Section>
            </Main>
        </div>
    </Layout>
)
